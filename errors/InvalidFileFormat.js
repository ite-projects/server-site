class InvalidFileFormat extends Error {
    constructor(message = null) {
        super();
        this.message = message || 'Attached file format is not supported';
    }
}

module.exports = InvalidFileFormat;