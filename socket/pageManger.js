const FileController = require('../controllers/fileController');
const Symmetric = require('../coreProject/symmetric');
const PGP = require('../coreProject/PGP');
const Signature = require('../coreProject/DigitalSignature');
const createResponse = require('../socket/response');
const CertificateHelper = require('../coreProject/certificate');

let myKeys;
let myCert;

function initKeyAndCert(){
    const pathPrKey = "./keys/me/private-key.pem";
    const pathPubKey = "./keys/me/public-key.pem";
    const pathCa = "./ssl/me/is-ca.pem";

    try {
        if (fs.existsSync(pathPrKey) && fs.existsSync(pathPubKey) && fs.existsSync(pathCa)) {
            console.log("all File need exist.")
            // load keys
            myKeys.private = fs.readFileSync("keys/me/private-key.pem", {encoding: "utf-8",});
            myKeys.public = fs.readFileSync("keys/me/public-key.pem", {encoding: "utf-8",});
            myCert = fs.readFileSync("sll/me/is-ca.pem", {encoding: "utf-8",});
        } else {
            myKeys = PGP.generateKeys();
            let csrPem = CertificateHelper.generatedCSR(myKeys.private, myKeys.public)
        }
    } catch(err) {
        console.error(err)
    }
}

// let publicKeys;
let clientsInfo = [];

const onConnection = (socket) => {
    // socket.id
    initKeyAndCert()
    socket.on('sendPublicKey', async function (res) {

        let {body, type} = res;

        body = decryptBodyRequest(type, body);

        clientsInfo.push({
            id: socket.id,
            publicKey: body.publicKey,
        });

        let data = createResponse.sendResponse({publicKey: myKeys.public});
        data.type = type;
        console.log('on sendPublicKey data = ', data);
        socket.emit('sendPublicKey', data)
    });

    socket.on('checkKey', async function (res) {
        let {body, type} = res;
        console.log('on checkKey res = ', res);

        try {
            body = decryptBodyRequest(type, body);
            clientsInfo.find((value) => value.id === socket.id).sessionKey = body.sessionKey;
            console.log("clientsInfo = ", clientsInfo);//todo remove

            let data = createResponse.sendResponse({handCheck: true});
            data.type = type;
            let clientPublicKey = clientsInfo.find((value) => value.id === socket.id).publicKey;
            data = encryptBodyResponse(type, data, clientPublicKey);
            socket.emit('checkStatus', data);

        }catch (e) {
            console.log("error decrypt RSA = ", e);
            let data = createResponse.sendResponse({handCheck: false});
            data.type = type;
            let clientPublicKey = clientsInfo.find((value) => value.id === socket.id).publicKey;
            data = encryptBodyResponse(type, data, clientPublicKey);
            socket.emit('checkStatus', data);
        }
    });

    socket.on('file.index', async function (res) {
        console.log("file.index");

        //parameter and request
        let {body, type} = res;
        if (type === 'pgp') {
            let currentUser = clientsInfo.find((value) => value.id === socket.id);
            const isVerify = verifySignature(currentUser.publicKey, body, res.sign);
            if (! isVerify){
                console.log("Invalid Signature")
                socket.emit('error', 'Invalid Signature');
                return;
            }

            let currentUserSessionKey = clientsInfo.find((value) => value.id === socket.id).sessionKey;
            body = decryptBodyRequest(type, body, currentUserSessionKey);
        }else {
            body = decryptBodyRequest(type, body);
        }

        //response
        var data = await FileController.getAll(body);

        if (data.status) {
            if (type === 'pgp') {
                let currentUserSessionKey = clientsInfo.find((value) => value.id === socket.id).sessionKey;
                data = encryptBodyResponse(type, data, currentUserSessionKey);
            }else {
                data = encryptBodyResponse(type, data);
            }
            socket.emit("file.list", data)
        } else {
            socket.emit('error', data.msg);
        }
    });

    socket.on('file.view', async function (res) {
        console.log("file.view");

        //parameter and request
        let {body, type} = res;
        if (type === 'pgp') {
            let currentUserSessionKey = clientsInfo.find((value) => value.id === socket.id).sessionKey;
            body = decryptBodyRequest(type, body, currentUserSessionKey);
        }else {
            body = decryptBodyRequest(type, body);
        }

        var data = await FileController.view(body);

        if (data.status) {
            if (type === 'pgp') {
                let currentUserSessionKey = clientsInfo.find((value) => value.id === socket.id).sessionKey;
                data = encryptBodyResponse(type, data, currentUserSessionKey);
            }else {
                data = encryptBodyResponse(type, data);
            }
            socket.emit("file.details", data)
        } else {
            socket.emit('error', data.msg);
        }
    });

    socket.on('file.update', async function (res) {

        console.log("file.update");

        //parameter and request
        let {body, type} = res;

        if (type === 'pgp') {
            let currentUserSessionKey = clientsInfo.find((value) => value.id === socket.id).sessionKey;
            body = decryptBodyRequest(type, body, currentUserSessionKey);
        }else {
            body = decryptBodyRequest(type, body);
        }

        let data = await FileController.update(body);

        if (data.status) {
            if (type === 'pgp') {
                let currentUserSessionKey = clientsInfo.find((value) => value.id === socket.id).sessionKey;
                data = encryptBodyResponse(type, data, currentUserSessionKey);
            }else {
                data = encryptBodyResponse(type, data);
            }
            socket.emit("file.edit", data)
        } else {
            socket.emit('error', data.msg);
        }
    });

};

module.exports = onConnection;

function decryptBodyRequest(type, body, key = null) {
    console.log("type Encrypt request= " + type);
    if (body) {
        switch (type) {
            case "none":
                break;
            case "sym":
                body = decryptSymmetric(process.env.SYMMETRIC_KEY, body);
                break;
            case "rsa":
                body = decryptRSA(myKeys.private, body);
                break;
            case "pgp":
                body = decryptSymmetric(key, body);
                break;
        }
        return JSON.parse(body);
    }
    return null;
}

function encryptBodyResponse(type, data, key = null) {
    console.log("type Encrypt response= " + type);
    data.type = type;
    if (data.body) {
        data.body = JSON.stringify(data.body);
        switch (type) {
            case "none":
                break;
            case "sym":
                data.body = encryptSymmetric(process.env.SYMMETRIC_KEY, data.body);
                break;
            case "rsa":
                data.body = encryptRSA(key, data.body);
                break;
            case "pgp":
                console.log("keyyy = ", key);
                data.body = encryptSymmetric(key, data.body);
                break;
        }
    }
    return data;
}

function encryptSymmetric(key, body) {
    console.log("===== encryptSymmetricBody =====");
    console.log("body before encrypt = ", body);
    body = Symmetric.encryptText(key, body);
    console.log("body after encrypt = ", body);
    console.log("===== end =====");
    console.log("===============\n");
    return body;
}

function decryptSymmetric(key, body) {
    console.log("===== decryptSymmetricBody =====");
    console.log("body before decrypt = ", body);
    body = Symmetric.decrypt(key, body);
    console.log("body after decrypt = ", body);
    console.log("===== end =====");
    console.log("===============\n");
    return body;
}

function encryptRSA(key, body) {
    console.log("===== encryptRSABody =====");
    console.log("body before encrypt = ", body);
    body = PGP.encrypt(key, body);
    console.log("body after encrypt = ", body);
    console.log("===== end =====");
    console.log("===============\n");
    return body;
}

function decryptRSA(key, body) {
    console.log("===== decryptRSABody =====");
    console.log("body before decrypt = ", body);
    body = PGP.decrypt(key, body);
    console.log("body after decrypt = ", body);
    console.log("===== end =====");
    console.log("===============\n");
    return body;
}

function verifySignature(public_key_client, message, signature) {
    return Signature.verify(public_key_client, message, signature);
}

