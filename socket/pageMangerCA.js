require('dotenv').config();
const createResponse = require('../socket/response');
const PGP = require('../coreProject/PGP');
const CertificateHelper = require('../coreProject/certificate');

async function onConnection(socket) {
    // socket.id

    await socket.on('connect', async function (res) {
        let myKeys = PGP.generateKeys();
        let csrPem = CertificateHelper.generatedCSR(myKeys.private, myKeys.public)

        let data = createResponse.sendResponse({csrPem: csrPem});
        socket.emit('requestCreateCertForServer', data);
    });

    await socket.on('sendCertToServer', async function (res, ackCallback) {
        let {body} = res;

        let certPem = body.certPem;
        console.log("certPem = ", certPem);
        socket.destroy()
        socket.emit('hhh', 'aa');
        ackCallback(certPem);
        // return  certPem;

        // myKeys = PGP.generateKeys();
        // let csrPem = CertificateHelper.generatedCSR(myKeys.private, myKeys.public)
        //
        // let data = createResponse.sendResponse({csrPem: csrPem});
        // socket.emit('requestCreateCertForServer', data);
    });

}

module.exports = {
    onConnection
};


