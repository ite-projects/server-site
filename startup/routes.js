require('express-async-errors');
const express = require('express');

const notFoundHandler = require('../middlewares/notFound');
const errorHandler = require('../middlewares/error');
const authEndPoints = require('../routes/auth');
const userEndPoints = require('../routes/user');
const fileEndPoints = require('../routes/file');


module.exports = function (app) {

    app.use(express.json());
    
    // Main endpoints
    app.use('/api/user', userEndPoints);
    app.use('/api/auth', authEndPoints);
    app.use('/api/file', fileEndPoints);

    // 404 handler
    app.use(notFoundHandler);
    
    // custom error handler
    app.use(errorHandler);

};