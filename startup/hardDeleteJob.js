const { Category } = require('../models/category');
const { Article } = require('../models/article');
const logger = require('../helpers/logger');

const hardDelete = () => {
    logger.info('Hard delete job triggerd ..');
    deleteArticles();
    deleteCategories();
}

const deleteCategories = async () => {
    const result = await Category.deleteMany({ isDeleted: true });

    if (result.ok == 1 && result.deletedCount > 0)
        logger.info(result.deletedCount + ' Categories have been deleted');
}

const deleteArticles = async () => {
    const result = await Article.deleteMany({ isDeleted: true });

    if (result.ok == 1 && result.deletedCount > 0)
        logger.info(result.deletedCount + ' Articles have been deleted');
}

module.exports = () => {
    setInterval(() => {
        hardDelete();
    }, 10000);
}