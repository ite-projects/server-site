const mongoose = require('mongoose');
// const config = require('config');
require('dotenv').config();
const logger = require('../helpers/logger');

module.exports = function() {

  // prepare connection string
  const connectionString = `${process.env.DATABASE_TYPE}://${process.env.DATABASE_HOST}:${process.env.DATABASE_PORT}
  /${process.env.DATABASE_NAME}`;

  // connect to database
  mongoose
    .connect(connectionString, {
      useUnifiedTopology: true,
      useNewUrlParser: true
    })
    .then(() => {
      logger.info(`Connected to ${process.env.DATABASE_NAME} successfuly ..`);
    });
};
