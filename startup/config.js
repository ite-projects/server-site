// const config = require('config');
require('dotenv').config();
const Joi = require('@hapi/joi');
const JoiObjectId = require('joi-objectid')(Joi);

module.exports = () => {

  // check if token secret key exist
  if (!process.env.JWT_KEY) {
    throw new Error('Token Secret key is not set!');
  }

  // extend Joi validator
  Joi.objectId = JoiObjectId;

};
