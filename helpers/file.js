// const config = require('config');
require('dotenv').config();

const fs = require('fs');

const photoRemover = (fileName) => {
    fs.unlink(process.env.STORAGE_PATH + '/' + fileName, (err) => {
        if (err) throw err;
    })
}

module.exports.photoRemover = photoRemover;