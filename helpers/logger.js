const { createLogger, transports, format } = require('winston');
require('dotenv').config();

const consoleTransport = new transports.Console({
    format: format.combine(
        format.colorize(),
        format.simple()
    )
});

const logger = createLogger({
    transports: [
        consoleTransport,
        new transports.File({ filename: process.env.DEFULT})
    ],
    exceptionHandlers: [
        consoleTransport,
        new transports.File({ filename: process.env.EXCEPTIONS })
    ]
});

module.exports = logger;