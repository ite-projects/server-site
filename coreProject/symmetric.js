const encryptionWithKeyAlgo = require('../components/encryptionWithKey');


function encryptText(key, text) {
    // return encryptionWithKeyAlgo.encryptText(Buffer.alloc(32, key, 'base64'), text)
    return encryptionWithKeyAlgo.encryptText(Buffer.from(key, 'base64'), text)
}

function decrypt(key, text) {
    // return encryptionWithKeyAlgo.decrypt(Buffer.alloc(32, key, 'base64'), text)
    return encryptionWithKeyAlgo.decrypt(Buffer.from(key, 'base64'), text)
}

function generateKey() {
    return encryptionWithKeyAlgo.generateKey().toString('base64')
}

module.exports = {
    generateKey,
    encryptText,
    decrypt,
};
