const NodeRSA = require('node-rsa');
const crypto = require('crypto');
const fs = require("fs");

function generateKeys() {
    let key = new NodeRSA();
    let keys = {};
    key.generateKeyPair(2048, 65537);
    keys.private = key.exportKey('pkcs8-private-pem');
    keys.public  = key.exportKey('pkcs8-public-pem');
    console.log('privateKey = ', keys.private);
    console.log('publicKey = ', keys.public);

    // Writing to files
    fs.writeFileSync("keys/me/private-key.pem", keys.private, {
        encoding: "utf-8", flag: 'w+'
    });
    fs.writeFileSync("keys/me/public-key.pem", keys.public, {
        encoding: "utf-8", flag: 'w+'
    });

    return keys;
}

function encrypt(publicKey, data) {
    let enc = crypto.publicEncrypt({
        key: publicKey,
        padding: crypto.RSA_PKCS1_OAEP_PADDING
    }, Buffer.from(data));

    return enc.toString('base64');
}

function decrypt(privateKey, data) {
    let enc = crypto.privateDecrypt({
        key: privateKey,
        padding: crypto.RSA_PKCS1_OAEP_PADDING
    }, Buffer.from(data, 'base64'));

    return enc.toString();
}

module.exports = {
    generateKeys,
    encrypt,
    decrypt,
};


//Example
// serverExampleEncrypt = () => {
//     console.log('Server public encrypting');
//
//     let enc = rsaWrapper.encrypt(rsaWrapper.serverPub, 'Server init hello');
//     console.log('Encrypted RSA string ', '\n', enc);
//     let dec = rsaWrapper.decrypt(rsaWrapper.serverPrivate, enc);
//     console.log('Decrypted RSA string ...');
//     console.log(dec);
// };
