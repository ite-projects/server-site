const forge = require("node-forge");
const pki = forge.pki;
require('dotenv').config();

function checkHaveCertificate() {

}

function generatedCSR(privateKey, publicKey){


    const prKey = pki.privateKeyFromPem(privateKey);
    const pubKey = pki.publicKeyFromPem(publicKey);


    // create a certification request (CSR)
    const csr = pki.createCertificationRequest();
    csr.publicKey = pubKey;
    csr.setSubject([
        {
            name: "commonName",
            value: "example.org",
        },
        {
            name: "countryName",
            value: "US",
        },
        {
            shortName: "ST",
            value: "Virginia",
        },
        {
            name: "localityName",
            value: "Blacksburg",
        },
        {
            name: "organizationName",
            value: "Test",
        },
        {
            shortName: "OU",
            value: "Test",
        },
    ]);

    // set (optional) attributes
    csr.setAttributes([
        {
            name: "challengePassword",
            value: "password",
        },
        {
            name: "unstructuredName",
            value: "My Company, Inc.",
        },
        {
            name: "extensionRequest",
            extensions: [
                {
                    name: "subjectAltName",
                    altNames: [
                        {
                            // 2 is DNS type
                            type: 2,
                            value: process.env.HOST,
                        },
                        {
                            type: 2,
                            value: process.env.HOST_IP,
                        },
                        {
                            type: 2,
                            value: "www.domain.net",
                        },
                    ],
                },
            ],
        },
    ]);

    // sign certification request
    csr.sign(prKey);

    // verify certification request
    const verified = csr.verify();

    console.log('verified = ', verified);

    // convert certification request to PEM-format
    const csrPem = pki.certificationRequestToPem(csr);

    // todo
    // get an attribute
    // csr.getAttribute({ name: "challengePassword" });

    // get extensions array
    // csr.getAttribute({ name: "extensionRequest" }).extensions;
    return csrPem;
}

module.exports = {
    generatedCSR,
}