require('dotenv').config();
const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http, {
    cors: {origin: "*", methods: "*"}
});
const cors = require('cors');
app.use(cors());


// configure
require('./startup/logging')();
require('./startup/config')();
require('./startup/routes')(app);
require('./startup/database')();

const onConnection = require('./socket/pageManger');

io.on('connection', onConnection);

http.listen(process.env.PORT, () => {
    console.log('listening on *:'+process.env.PORT);
});

module.exports = {
    io,
};


// var socket = io.connect(`http://${process.env.HOST_IP}:${process.env.PORT_CA}`);
// onConnection.onConnection(socket);

// (async () => {
//     try {
//         var io2 = require('socket.io-client')
//         var socket = io2.connect('http://192.168.1.108:3002');
//         let certPem = await onConnection.onConnection(socket, io2);
//         console.log("wwwww", 'asd');
//         console.log("certPem = ", certPem);
//     } catch (e) {
//         // Deal with the fact the chain failed
//     }
// })();




