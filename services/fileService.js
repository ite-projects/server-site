const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const FileModel = require('../models/file');
const responseSocket = require('../socket/response');
const { successResponse, errorResponse } = require('../helpers/restful-responses');


module.exports = {
    index,
    view,
    update,
};


async function index()  {
    try {
        const files = await FileModel.schemeModelFile.find()
        return responseSocket.sendResponse(files);
    } catch (err) {
        return responseSocket.sendError(err);
    }
}

async function view(req) {
    const content = fs.readFileSync(path.resolve("upload", req.name)).toString();

    console.log(content);
    return responseSocket.sendResponse({name: req.name, text: content});
}

async function update(req) {

    const exists = fs.existsSync(path.resolve('upload', req.name));

    try {
        fs.writeFileSync(path.resolve('upload', req.name), req.text, {flag: "w+"});
    }catch (e) {
        console.log(e);
        return responseSocket.sendError("file can't found and can't create");
    }

    if (! exists){
        var newFile = new FileModel.schemeModelFile(_.pick(req, [
            'name',
            'text'
        ]));

        // newFile.createdBy = req.user._id;

        //save
        await newFile.save();
    }
    return responseSocket.sendResponse({name: req.name, text: req.text});
}