const bcrypt = require('bcrypt');
const _ = require('lodash');
const UserModel = require('../models/user');
const { successResponse, errorResponse } = require('../helpers/restful-responses');

module.exports = {
    create,
    login,
    me,
};

async function create(req, res) {

    const newUser = new UserModel.schemeModelUser(_.pick(req.body, [
        'name',
        'email',
        'password'
    ]));
    // hash password
    const salt = await bcrypt.genSalt(10);
    newUser.password = await bcrypt.hash(newUser.password, salt);

    //save user
    await newUser.save();

    // generate token
    const token = UserModel.generateAuthToken(newUser);

    object_token = {'token' : token};
    res.send(
        successResponse(
            UserModel.getExposedAttributes(newUser, object_token)
        )
    );
}

async function login(req, res) {

    const user = await UserModel.schemeModelUser.findOne({ email: req.body.email });
    if (! user)
        return res.status(400)
            .send(errorResponse("Invalid email or password."));

    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if (! validPassword)
        return res.status(400)
            .send(errorResponse('Invalid email or password.'));

    const token = UserModel.generateAuthToken(user);

    object_token = {'token' : token};
    res.send(
        successResponse(
            UserModel.getExposedAttributes(user, object_token)
        )
    );
}


async function me(req, res) {
    const user = await UserModel.schemeModelUser.findById(req.user._id);
    res.send(
        successResponse(
            UserModel.getExposedAttributes(user)
        )
    );
}