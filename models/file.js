const mongoose = require('mongoose');
const Joi = require('@hapi/joi');
// const config = require('config');
const dotenv = require('dotenv').config();
const jwt = require('jsonwebtoken');
const _ = require('lodash');

const fileScheme = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 3,
        max: 50
    },
    createdBy: {
        type: String,
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

// define exposed attributes
fileScheme.statics.exposed = [
    '_id',
    'server',
    'port',
    'name',
    'createdAt'
];


fileScheme.methods.getExposedAttributes = function (append_object1 = {}) {
    if (typeof append_object1 !== 'object'){
        console.log("error type of object");
        return null;
    }
    return _.assign(append_object1, _.pick(this, this.constructor.exposed))
};


const schemeModelFile = mongoose.model('files', fileScheme);

validateIndex = function(req) {
    var schema =  Joi.object({
        server: Joi.string().required().min(3).max(50),
        port: Joi.number().integer().required().min(1000).max(9999),
    }).options({
        abortEarly: false
    });
    return schema.validate(req);
};


validateUpdate = function(req) {
    var schema = Joi.object({
        server: Joi.string().required().min(3).max(50),
        port: Joi.number().integer().required().min(1000).max(9999),
        name: Joi.string().required().min(3).max(50),
        text: Joi.string().required().max(500),
    });
    return schema.validate(req, {
        abortEarly: false
    });
};

validateView = function(req) {
    var schema = Joi.object({
        server: Joi.string().required().min(3).max(50),
        port: Joi.number().integer().required().min(1000).max(9999),
        name: Joi.string().required().min(3).max(50),
    });
    return schema.validate(req, {
        abortEarly: false
    });
};


module.exports = {
    schemeModelFile,
    validateIndex,
    validateUpdate,
    validateView,
};
