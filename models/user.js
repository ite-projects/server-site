const mongoose = require('mongoose');
const Joi = require('@hapi/joi');
// const config = require('config');
const dotenv = require('dotenv').config();
const jwt = require('jsonwebtoken')
const _ = require('lodash');

const userScheme = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 3,
        max: 50
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

// define exposed attributes
var exposed = [
    '_id',
    'name',
    'email',
    'createdAt'
];


getExposedAttributes = function (user ,append_object1 = {}) {
    if (typeof append_object1 !== 'object'){
        console.log("error type of object");
        return null;
    }
    return _.assign(append_object1, _.pick(user, exposed))
};

generateAuthToken = function(user) {
    return jwt.sign({ _id: user._id }, process.env.JWT_KEY);
};


const schemeModelUser = mongoose.model('User', userScheme);

validateRegister = function(req) {
    var schema =  Joi.object({
        name: Joi.string().required().min(3).max(50),
        email: Joi.string().email({ tlds: { allow: false } }).required(),
        password: Joi.string().min(8).max(30).required(),
        createdAt: Joi.date(),
        // transactions: Joi.array().unique('email').required(),
    }).options({
        abortEarly: false
    });
    return schema.validate(req);
};

validateLogin = function(req) {
    var schema = Joi.object({
        email: Joi.string().email({ tlds: { allow: false } }).required(),
        password: Joi.string().min(8).max(30).required(),
    });
    return schema.validate(req, {
        abortEarly: false
    });
};

module.exports = {
    schemeModelUser,
    getExposedAttributes,
    generateAuthToken,
    validateRegister,
    validateLogin
};
