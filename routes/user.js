const express = require('express');
const router = express.Router();
const UserController = require('../controllers/userController');
const Auth = require('../middlewares/auth');
const { successResponse, errorResponse } = require('../helpers/restful-responses');

// Endpoints
router.get('/me', Auth, async (req, res) => {
    UserController.showMe(req, res);
});

module.exports = router;