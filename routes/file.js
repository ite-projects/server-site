const express = require('express');
const router = express.Router();
const FileController = require('../controllers/fileController');
const Auth = require('../middlewares/auth');
const FileExists = require('../middlewares/fileExists');


// Endpoints
router.get('/', Auth, FileController.validateGetAll, async (req, res) => {
    FileController.getAll(req, res);
});

router.post('/update', Auth, FileController.validateUpdate, async (req, res) => {
    FileController.update(req, res);
});

router.get('/show', Auth, FileController.validateView, FileExists, async (req, res) => {
    FileController.view(req, res);
});


module.exports = router;