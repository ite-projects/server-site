const InvalidFileFormat = require('../errors/InvalidFileFormat');
const { errorResponse } = require('../helpers/restful-responses');
const logger = require('../helpers/logger');

module.exports = (err, req, res, next) =>  {
    if(err instanceof InvalidFileFormat) {
        return res.status(400).send(
            errorResponse(err.message)
        );
    }

    // log the error
    logger.error(err.message, err);
    res.status(500).send(
        errorResponse('Internal server error.')
    );
}