const jwt = require('jsonwebtoken');
require('dotenv').config();
const { errorResponse } = require('../helpers/restful-responses');

module.exports = (req, res, next) => {
    const token = req.header('Authorization');
    if (! token) {
        return res
          .status(401)
          .send(errorResponse('Access Denied. Token not provided!'));
    }

    try {
        const payload = jwt.verify(token, process.env.JWT_KEY );
        req.user = payload;
        next();
    } catch (ex) {
        res.status(400).send(errorResponse('Invalid token.'));
    }
};