const FileModel = require('../models/file');
const FileService = require('../services/fileService');
const { errorResponse } = require('../helpers/restful-responses');
const _ = require('lodash');

module.exports = {
    validateGetAll,
    validateUpdate,
    validateView,
    getAll,
    update,
    view,
};


//getAll
function validateGetAll(req, res, next) {
    const response = FileModel.validateIndex(req.body);
    if (response.error) {
        const errors = _.map(
            response.error.details,
            e => e.message
        );
        return res.status(400).send(
            errorResponse(errors)
        );
    }
    console.log("validateGetAll next");
    next();
}

async function getAll(req) {
    return await FileService.index(req);
}


//update
function validateUpdate(req, res, next) {
    console.log("validateUpdate");
    const response = FileModel.validateUpdate(req.body);
    if (response.error) {
        const errors = _.map(
            response.error.details,
            e => e.message
        );

        return res.status(400).send(
            errorResponse(errors)
        );
    }
    next();
}

async function update(req) {
    return await FileService.update(req);
}


//View
function validateView(req, res, next) {
    const response = FileModel.validateView(req.body);
    if (response.error) {
        const errors = _.map(
            response.error.details,
            e => e.message
        );

        return res.status(400).send(
            errorResponse(errors)
        );
    }
    next();
}

async function view(req) {
    return await FileService.view(req);
}