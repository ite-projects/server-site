const { errorResponse } = require('../helpers/restful-responses');
const UserModel = require('../models/user');
const UserService = require('../services/userService');
const _ = require('lodash');


module.exports = {
    register,
    login,
};

async function register(req, res) {

    const response = UserModel.validateRegister(req.body);
    if (response.error) {
        const errors = _.map(
            response.error.details,
            e => e.message
        );

        return res.status(400).send(
            errorResponse(errors)
        );
    }

    UserService.create(req, res);
}


async function login(req, res) {

    const response = UserModel.validateLogin(req.body);
    if (response.error) {
        const errors = _.map(
            response.error.details,
            e => e.message
        );

        return res.status(400).send(
            errorResponse(errors)
        );
    }

    UserService.login(req, res);
}