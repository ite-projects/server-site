const UserService = require('../services/userService');
const _ = require('lodash');


module.exports = {
    showMe,
};

async function showMe(req, res) {
    UserService.me(req, res);
}
